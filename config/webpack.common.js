var path = require('path');

var ROOT_PATH = path.resolve(__dirname);
var APP_PATH = path.join(ROOT_PATH, '../src/app.js');
var BUILD_PATH = path.join(ROOT_PATH, '../build');

const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");


module.exports = {
    entry: {
        app: [APP_PATH]
    },
    output: {
		path: BUILD_PATH,
        filename: 'bundle.js'
    },
    resolve : {
        alias: {
            'jquery': path.join(__dirname, '../node_modules/jquery/src/jquery')
        }
    },
    module: {
        rules: [
            // JavaScript: Use Babel to transpile JavaScript files
            {
                test: /\.(js|jsx)$/, exclude: /node_modules/, use: ["babel-loader"]
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: { sourceMap: true, importLoaders: 1 }
                    },
                    { loader: "sass-loader", options: { sourceMap: true } }
                ],
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: "asset/resource"
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: "asset/inline"
            }
        ]
    },
    plugins: [
        // Removes/cleans build folders and unused assets when rebuilding
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: "./src/images",
                    to: "images"
                }
            ]
        })
    ]
}