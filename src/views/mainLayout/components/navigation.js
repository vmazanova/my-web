import React from 'react';
import { Link, NavLink } from 'react-router-dom';

class Navigation extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<nav role="navigation" className="purple darken-2">
				<div className="nav-wrapper container">
					<Link to="/" className="brand-logo">MAZANOVA.SK</Link>
					<ul className="right hide-on-med-and-down">
						<li><NavLink exact to="/" activeClassName="active">Home</NavLink></li>
						<li><NavLink to="/about" activeClassName="active">About</NavLink></li>
						<li><NavLink to="/projects" activeClassName="active">Projects</NavLink></li>
						<li><NavLink to="/articles" activeClassName="active">Articles</NavLink></li>
						<li><NavLink to="/contact" activeClassName="active">Contact</NavLink></li>
					</ul>
					<ul id="nav-mobile" className="sidenav">
						<li><NavLink exact to="/" activeClassName="active">Home</NavLink></li>
						<li><NavLink to="/about" activeClassName="active">About</NavLink></li>
						<li><NavLink to="/projects" activeClassName="active">Projects</NavLink></li>
						<li><NavLink to="/articles" activeClassName="active">Articles</NavLink></li>
						<li><NavLink to="/contact" activeClassName="active">Contact</NavLink></li>
					</ul>
					<a href="#" data-target="nav-mobile" className="sidenav-trigger"><i className="material-icons">menu</i></a>
				</div>
			</nav>
		);
	}
}

export default Navigation;
