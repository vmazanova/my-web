import React from 'react';
import { Helmet } from "react-helmet";

import Navigation from './components/navigation'


class MainLayout extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		$('.sidenav').sidenav();
	}
	
	render() {
		return (
			<div className="app">
				<Helmet>
					<title>Mazanova - Personal page</title>
				</Helmet>
				<Navigation />

				<main className="container">
					{this.props.children}
				</main>

				<footer className="page-footer grey darken-4">
					<div className="container">
						<div className="row footer-links">
							<a href="https://www.facebook.com/v.nika.mazanova" target="_blank" rel="noopener noreferrer">
								<img className="flat-icon" src="/images/icons/facebook.png" alt="Facebook-icon"/>
							</a>
							<a href="http://sk.linkedin.com/in/veronikamazanova" target="_blank" rel="noopener noreferrer">
								<img className="flat-icon" src="/images/icons/linkedin.png" alt="LinkedIn-icon"/>
							</a>
							<a href="https://bitbucket.org/vmazanova" target="_blank" rel="noopener noreferrer">
								<img className="flat-icon" src="/images/icons/bitbucket.png" alt="Bitbucket-icon"/>
							</a>
						</div>
					</div>
					<div className="footer-copyright">
						<div className="container">
							Copyright © 2022 <b>Mazanova</b>. All rights reserved.
						</div>
					</div>
				</footer>
			</div>
		);
	}
}

export default MainLayout;
