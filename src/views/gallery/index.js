import React from 'react';
import 'react-photoswipe/lib/photoswipe.css';
import {PhotoSwipeGallery} from 'react-photoswipe';


const Gallery = (props) => {

    const getThumbnailContent = (item) => {
		return (
			<img src={item.thumbnail} height={120}/>
		);
	}

    return (
        <PhotoSwipeGallery items={props.images} options={{}} thumbnailContent={getThumbnailContent}/>
    );
}

export default Gallery;