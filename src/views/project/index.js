import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import Gallery from '../gallery';


const Project = () => {
	const iconsDict = {
		source: "icons/package.png",
		conference: "icons/conference.png",
		tutorial: "icons/tutorial.png",
		exe: "icons/play.png"
	}

	let { projectId } = useParams();
	const [project, setProject] = useState({
		longDescription: [],
		linkCategories: [],
		implementation: [],
		images: []
	});

	useEffect(() => {
		const fetchProject = async () => {
			const response = await fetch(SERVER_URL + "/server/data/projects/" + projectId + ".json");
			const json = await response.json();
			setProject(json);
		}

		fetchProject();
	}, [projectId]);

	useEffect(() => {
		$('.collapsible').collapsible({
			accordion : false
		});
	})

	return (
		<div className="project">
			<h1>{project.title}</h1>
			<div className="row">
				<div className="col s12 m12 l8">
					{project.longDescription.map((desc, idx) =>
						<p key={idx}>{desc}</p>
					)}
				</div>
				<div className="col s12 m6 l4">
					<ul className="collapsible">
						{project.linkCategories.map((category) =>
							<li key={category.type} className="active">
								<div className="collapsible-header">
									<img src={"/images/" + iconsDict[category.type]} alt={category.type + " icon"}/>
									{category.name}
								</div>
								<div className="collapsible-body">
									<div className="collection">
										{category.items.map((item, idx) =>
											<a key={idx} href={item.url} className="amber-link collection-item">
												{item.name}
											</a>
										)}
									</div>
								</div>
							</li>
						)}
					</ul>
					{project.implementation.map((language, idx) =>
						<div key={idx} className="chip purple darken-2 white-text z-depth-1">
							<img src={"/images/logos/" + language.file} alt={language.name + " logo"}/>
							{language.name}
						</div>
					)}
				</div>
			</div>
			{project.images && 
				<div id="gallery" className="section my-divider">
					<h2>Gallery</h2>
					<Gallery images={project.images}/>
				</div>
			}
		</div>
	);
}

export default Project;
