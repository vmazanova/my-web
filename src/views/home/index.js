import React from 'react';

class Home extends React.Component {
	render() {
		return (
			<div className="home-page">
				<div className="welcome-container">
					<img src="images/avatar.png" alt="Avatar"/>
					<h1>Veronika Mazánová</h1>
					<p>Welcome to my personal web site.</p>
				</div>
			</div>
		);
	}
}

export default Home;
