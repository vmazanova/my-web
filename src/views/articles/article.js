import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ReactMarkdown from 'react-markdown'

import Gallery from '../gallery';
import { Helmet } from "react-helmet";

 
const Article = () => {
    let { articleId } = useParams();
    const [jsonContent, setJsonContent] = useState({
        "title": '',
        "body": []
    });
  
    useEffect(() => {
        fetch(SERVER_URL + "/server/data/articles/" + articleId + ".json")
            .then((response) => response.json())
            .then((data) => setJsonContent(data));
    }, [articleId]);
  
    return (
        <div className="article">
            <Helmet>
                <title>{"Mazanova - " + jsonContent.shortTitle}</title>
                <meta property="og:title" content={jsonContent.title} />
                <meta property="og:type" content="article" />
                <meta property="og:image" content={SERVER_URL + "/images/" + jsonContent.thumbnail} />
                <meta property="og:url" content={window.location.href} />
                <meta property="og:description" content={jsonContent.description} />
            </Helmet>
            <h1>{jsonContent.title}</h1>
            {jsonContent.body.map((section, idx) => {
                if ('markdown' in section) {
                    return <ReactMarkdown key={idx}>{section.markdown}</ReactMarkdown>;
                } else if ('images' in section) {
                    return <Gallery key={idx} images={section.images}/>;
                } else {
                    return null;
                }
            })}
        </div>
    );
}
 
export default Article;