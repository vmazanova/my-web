import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
 

const Articles = () => {
    const [articles, setArticles] = useState([]);
  
    useEffect(() => {
        fetch("/server/data/all-articles.json")
            .then((response) => response.json())
            .then((data) => setArticles(data.articles))
    }, []);
  
    return (
        <div>
            <h1>My articles</h1>
            <p>On this page you can find all my articles that I have written.</p>
            <div className="row">
                {articles.map((article) => 
                    <div key={article.id} className="col s12 m6 l4">
                        <div className="card hoverable purple darken-2">
                            <div className="card-image waves-effect waves-block waves-light">
                                <Link to={"/article/" + article.id} ><img src={"/images/" + article.thumbnail}/></Link>
                            </div>
                            <div className="card-content white-text">
                                <div className="card-title">
                                    <Link to={"/article/" + article.id} className="white-text line-clamp">{article.title}</Link>
                                </div>
                            </div>
                            <div className="card-action">
                                <Link className="amber-link" to={"/article/" + article.id}>View article</Link>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
 
export default Articles;