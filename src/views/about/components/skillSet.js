import React from 'react';

class SkillSet extends React.Component {
	constructor() {
		super();

		var skillValues = [
			{
				language: "JavaScript",
				percentage: "75%"
			},
			{
				language: "React",
				percentage: "65%"
			},
			{
				language: "HTML",
				percentage: "60%"
			},
			{
				language: "CSS",
				percentage: "60%"
			},
			{
				language: "Angular",
				percentage: "60%"
			},
			{
				language: "C++",
				percentage: "50%"
			},
			{
				language: "Python",
				percentage: "50%"
			},
			{
				language: "Unity",
				percentage: "40%"
			},
			{
				language: "C",
				percentage: "40%"
			},
			{
				language: "OpenCV",
				percentage: "40%"
			},
			{
				language: "Java",
				percentage: "30%"
			},
		];
		
		this.state = {
			'skillLevels': ["Fundamental Awareness", "Novice", "Intermediate", "Advanced", "Expert"],
			'skillValues': skillValues,
			'otherSkills': ["Webpack", "Node.js", "jQuery", "MySQL", "Sass", "Git", "Jira", "Microsoft Azure"]
		 };
	}
	
	render() {
		return(
			<div id="skillset" className="section my-divider">
				<h2>My skillset</h2>
				<div className="skillset">
					<div className="skill-chart">
						<div className="row">
							<div className="col s8 offset-s3 m5 offset-m4">
								<div className="skill-levels">
									{this.state.skillLevels.map((label, idx) =>
										<div key={idx} className="pin">
											<span className="rotated-label">
												{label}
											</span>
										</div>
									)}
								</div>
							</div>
						</div>
						<div className="skill-values">
							{this.state.skillValues.map((skill, idx) =>
								<div key={idx} className="row">
									<div className="col s3 m2 offset-m2 l1 offset-l3 language">
										{skill.language}
									</div>
									<div className="col s8 m5 progress">
										<div className="determinate" style={{width: skill.percentage}}></div>
									</div>
								</div>
							)}
						</div>
					</div>
					<div className="row other-skills">
						<div className="col l6 m7 offset-m2 s11 offset-l3">
							{this.state.otherSkills.map((skill, idx) =>
								<div key={idx} className="chip purple darken-2 white-text z-depth-1">{skill}</div>
							)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default SkillSet;
