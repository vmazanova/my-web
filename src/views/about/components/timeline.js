import React from 'react';

class Timeline extends React.Component {
	constructor() {
		super();
		
		this.state = { 
			experience: [],
			icons: {
				work: "icons/briefcase.png",
				school: "icons/school-hat.png"
			}
		}
		
		this.shortenText = this.shortenText.bind(this);
	}
	
	shortenText() {
		var showChar = 100;
		var ellipsestext = "...";
		var moretext = "more";
		var lesstext = "less";
		$('.more').each(function() {
			var content = $(this).html();
			
			if (content.length > showChar) {
				var c = content.substr(0, showChar);
				var h = content.substr(showChar);
				
				var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
				
				$(this).html(html);
			}
		});
		
		$(".morelink").on("click", function() {
			if ($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
			
			return false;
		});
	}
	
	componentDidUpdate() {
		this.shortenText();
	}
	
	componentDidMount() {
		var _this = this;
		
		$.ajax({
			url: "/server/data/experience.json",
			dataType: 'json',
			type: 'GET',
		}).done(function (data) {
			_this.setState({
				experience: data.experience
			});
		}).fail(function (jqXHR, status, err) {
			console.error("experience.json", status, err.toString());
		});
	}
	
	render() {
		var _this = this;
		
		return(
			<div id="timeline" className="section my-divider">
				<h2>Timeline</h2>
				<div className="timeline">
					{this.state.experience.map((exp, idx) =>
						<div key={idx} className="timeline-block">
							<div className={"timeline-img " + exp.type + " z-depth-4"}>
								<img src={"/images/" + _this.state.icons[exp.type]} alt={exp.type + " icon"} />
							</div>
							<div className="row">
								<div className="timeline-content col s10 l5 offset-s2">
									<div className="card hoverable purple darken-2">
										<div className="card-content white-text">
											<div className="card-title">{exp.position}</div>
											<div className="description">
												{exp.description.map((item, idx) =>
													<div key={idx} className="description-item">
														<b>{item.label}: </b>
														<span className="more">{item.content}</span>
													</div>
												)}
											</div>
										</div>
										<div className="card-action">
											{exp.links && exp.links.map((link, idx) =>
												<a key={idx} className="amber-link" href={link.url}>{link.name}</a>
											)}
										</div>
									</div>
									<span className="date">{exp.date}</span>
								</div>
							</div>
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default Timeline;
