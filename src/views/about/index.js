import React from 'react';
import SkillSet from './components/skillSet'
import Timeline from './components/timeline'

class About extends React.Component {
	render() {
		return (
			<div className="about">
				<h1>About me</h1>
				<p>I'm a software developer deeply interested in learning new things and how do they work. Here you can look up my technical skills in form of a bar chart while my work experience and education is visualized by a vertical timeline.</p>
				<SkillSet/>
				<Timeline/>
			</div>
		);
	}
}

export default About;
