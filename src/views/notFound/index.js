import React from 'react';

class NotFound extends React.Component {
	render() {
		return (
			<div className="card-panel red darken-4 white-text error">
				<img src="/images/icons/error.png" alt="Error-img"/>
				<h1>Error 404</h1>
				<p>The page you are looking for doesn't exist.</p>
			</div>
		);
	}
}

export default NotFound;
