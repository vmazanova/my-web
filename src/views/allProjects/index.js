import React from 'react';
import { Link } from 'react-router-dom';


class AllProjects extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = { projects: [] }
		
		this.capitalizeFirstLetter = this.capitalizeFirstLetter.bind(this);
		this.indexOfGroup = this.indexOfGroup.bind(this);
		this.groupProjects = this.groupProjects.bind(this);
	}
	
	componentDidMount() {
		var _this = this;
		
		$.ajax({
			url: "/server/data/all-projects.json",
			dataType: 'json',
			type: 'GET',
		}).done(function (data) {
			_this.setState({
				projects: data.projects
			});
		}).fail(function (jqXHR, status, err) {
			console.error("all-projects.json", status, err.toString());
		});
	}
	
	capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
			
	indexOfGroup(projects, targetGroup) {
		for (var i = 0; i < projects.length; i++) {
			if (projects[i].name === targetGroup) {
				return i;
			}
		}

		return -1;
	}
		
	groupProjects(projects) {
		var groupedProjects = [];
		
		for (var i = 0; i < projects.length; i++) {
			var projectType = projects[i].type.toLowerCase();
			var idx = this.indexOfGroup(groupedProjects, projectType);
			
			if (idx >= 0) {
				groupedProjects[idx].projects.push(projects[i]);
			} else {
				groupedProjects.push({
					name: projectType,
					projects: [projects[i]]
				});
			}
		}
		
		return groupedProjects;
	}

	render() {
		var _this = this;
		var groupedProjects = this.groupProjects(this.state.projects);
		
		return (
			<div className="all-projects">
				<h1>My projects</h1>
				<p>On this page you can find all my projects that I'm proud of. It includes my games, webs and school projects.</p>
				{groupedProjects.map(function(group, index) {
					var groupName = _this.capitalizeFirstLetter(group.name).split('-').join(' ');
					return (
						<div key={group.name} className={"section" + (index > 0 ? " my-divider" : '')}>
							<h2>{groupName}</h2>	
							<div className="row">
								{group.projects.map((project) =>
									<div key={project.id} className="col s12 m6 l4">
										<div className="card hoverable purple darken-2">
											<div className="card-image waves-effect waves-block waves-light">
												<Link to={"/project/" + project.id} ><img src={"/images/" + project.thumbnail}/></Link>
											</div>
											<div className="card-content white-text">
												<div className="card-title">
													<Link to={"/project/" + project.id} className="white-text line-clamp">{project.title}</Link>
												</div>
												<p>{project.description}</p>
											</div>
											<div className="card-action">
												<Link className="amber-link" to={"/project/" + project.id}>View project</Link>
											</div>
										</div>
									</div>
								)}
							</div>
						</div>
					);
				})}
			</div>
		);
	}
}

export default AllProjects;
