import React from 'react';
import ContactForm from './components/contactForm';
import Alert from './components/alert';

class Contact extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			alert: false
		};
		
		this.hideAlert = this.hideAlert.bind(this);
		this.setAlertState = this.setAlertState.bind(this);
		this.handleMessageSubmit = this.handleMessageSubmit.bind(this);
	}
	
	hideAlert() {
		this.setAlertState(false);
	}
	
	setAlertState(alertObj) {
		this.setState({
			alert: alertObj
		});
	}
	
	handleMessageSubmit(message) {
		var _this = this;
		
		return new Promise(function (resolve, reject) {
			$.ajax({
				url: "/server/mail/mailform.php",
				dataType: 'json',
				type: 'POST',
				data: message
			}).done(function (data) {
				if (data.success) {
					_this.setAlertState({
						type: "success",
						message: "Message sent successfully!"
					});
					
					resolve();
				} else {
					_this.setAlertState({
						type: "error",
						message: "Message could not be send. Response from the server: " + data.error
					});
					
					reject();
				}
			}).fail(function (jqXHR, status, err) {
				console.error("mailform.php", status, err.toString());
				
				_this.setAlertState({
					type: "error",
					message: "Message could not be send. Was not able to contact server, reason: " + err.toString()
				});
				
				reject();
			});
		});
	}
	  
	render() {
		var alert = this.state.alert ? <Alert type={this.state.alert.type} message={this.state.alert.message} onAlertHide={this.hideAlert}/> : '';
		
		return (
			<div className="contact">
				<h1>Contact me</h1>
				<p>If you want to get in touch with me, you can either use the following contact form or social media that are in the footer.</p>
				{alert}
				<ContactForm onMessageSubmit={this.handleMessageSubmit}/>
			</div>
		);
	}
}

export default Contact;
		