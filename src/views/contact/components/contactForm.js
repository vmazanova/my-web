import React from 'react';

require('jquery-validation');

class ContactForm extends React.Component {
	constructor(props) {
		super(props);

		this.nameInputRef = React.createRef();
		this.emailInputRef = React.createRef();
		this.subjectInputRef = React.createRef();
		this.textInputRef = React.createRef();
		this.captchaInputRef = React.createRef();
	}
	
	componentDidMount() {
		var _this = this;
		
		$('form').on("submit", function(e) {
			e.preventDefault();
		}).validate({
			errorClass: 'invalid',
			errorPlacement: function (error, element) {
				element.siblings(".helper-text").attr("data-error", error.contents().text());
				element.next("label").addClass("active");
			},
			submitHandler: function(form) {
				_this.props.onMessageSubmit({
					name: _this.nameInputRef.current.value,
					email: _this.emailInputRef.current.value,
					subject: _this.subjectInputRef.current.value,
					text: _this.textInputRef.current.value,
					captcha: _this.captchaInputRef.current.value,
				}).then(function() {
					form.reset();
				}).catch(function() {
					console.error("message failed - form won't reset");
				});
		
				// request new captcha
				$("#captcha-img").attr("src", "/server/mail/captcha.php?" + (new Date()).getTime());
				
				return false;
			}
        });
	}
	
	render() {
		return (
			<div className="contact-form">
				<div className="row">
					<form className="col s12">
						<div className="row">
							<div className="input-field col s6 m3">
								<input id="name" name="name" type="text" className="validate"
									required minLength="3" maxLength="50"
									ref={this.nameInputRef} />
								<label htmlFor="name">Name</label>
								<span className="helper-text"></span>
							</div>
							<div className="input-field col s6 m3">
								<input id="email" name="email" type="email" className="validate" required
									ref={this.emailInputRef} />
								<label htmlFor="email">Email</label>
								<span className="helper-text"></span>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s6">
								<input id="subject" name="subject" type="text" className="validate"
									required minLength="3" maxLength="50"
									ref={this.subjectInputRef} />
								<label htmlFor="subject">Subject</label>
								<span className="helper-text"></span>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<textarea id="textarea1" name="textarea1" className="materialize-textarea validate"
									required minLength="3" maxLength="500"
									ref={this.textInputRef}></textarea>
								<label htmlFor="textarea1">Textarea</label>
								<span className="helper-text"></span>
							</div>
						</div>
						<div className="row">
							<div className="col s6 m3 l2">
								<img id="captcha-img" src="/server/mail/captcha.php"/>
							</div>
							<div className="input-field col s6 m3">
								<input id="captcha" name="captcha" type="text" className="validate" required
									ref={this.captchaInputRef} />
								<label htmlFor="captcha">Captcha</label>
								<span className="helper-text"></span>
							</div>
						</div>
						<button className="btn waves-effect waves-light" type="submit" name="action">Submit
							<i className="material-icons right">send</i>
						</button>
					</form>
				</div>
			</div>
		);
	}
}

export default ContactForm;
