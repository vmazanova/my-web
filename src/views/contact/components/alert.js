import React from 'react';
import ReactDOM from 'react-dom';

var classNames = require('classnames');

class Alert extends React.Component {
	constructor(props) {
		super(props);
	}
	
	capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	render() {
		if (this.props.type == "success") {
			setTimeout(function() {
				this.props.onAlertHide();
			}.bind(this), 5000);
		}
		
		var cardColor = classNames({
			'card': true,
			'red darken-4': this.props.type == "error",
			'green darken-1':  this.props.type == "success"
		});
		
		return (
			<div className="row">
				<div className="col s12">
					<div className={cardColor}>
						<div className="card-content white-text">
							<span className="card-title">{this.capitalizeFirstLetter(this.props.type)}</span>
							<p>{this.props.message}</p>
						</div>
						<div className="card-action">
							<a href="javascript:;" className="amber-link" onClick={this.props.onAlertHide}>Close</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Alert;
