import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ReactGA from 'react-ga';
ReactGA.initialize('UA-84325328-1');

import MainLayout from './views/mainLayout/index';

import Home from './views/home/index';
import About from './views/about/index';
import AllProjects from './views/allProjects/index';
import Project from './views/project/index';
import Articles from './views/articles/index';
import Article from './views/articles/article';
import Contact from './views/contact/index';
import NotFound from './views/notFound/index';

function logPageView() {
	ReactGA.set({ page: window.location.pathname });
	ReactGA.pageview(window.location.pathname);
	return null;
}

export default (
	<BrowserRouter>
		<Route component={logPageView} />
		<MainLayout>
			<Switch>
				<Route exact path="/">
					<Home />
				</Route>
				<Route path="/about">
					<About />
				</Route>
				<Route path="/projects">
					<AllProjects />
				</Route>
				<Route path="/project/:projectId">
					<Project />
				</Route>
				<Route path="/articles">
					<Articles />
				</Route>
				<Route path="/article/:articleId">
					<Article />
				</Route>
				<Route path="/contact">
					<Contact />
				</Route>
				<Route>
					<NotFound />
				</Route>
			</Switch>
		</MainLayout>
  </BrowserRouter>
);