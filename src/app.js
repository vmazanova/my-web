require('./style.scss');
require('materialize-css/dist/js/materialize.min.js');

import React from 'react';
import ReactDOM from 'react-dom';

import Router from './router';

ReactDOM.render(Router, document.getElementById('root'));