<?php
 
function return_json($arr)
{
	unset($_SESSION['captcha']);
    die(json_encode($arr));
}
 
session_start();
 
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_SESSION['captcha'])
    && isset($_POST['name'])
    && isset($_POST['email'])
    && isset($_POST['subject'])
    && isset($_POST['text'])
    && isset($_POST['captcha']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $text = $_POST['text'];
    $captcha = $_POST['captcha'];
   
    if (strlen($name) < 3 || strlen($name) > 30)
    {
        return_json(array('success' => false, 'error' => 'invalid_name'));
    }
   
    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        return_json(array('success' => false, 'error' => 'invalid_email'));
    }
   
    if (strlen($subject) < 3 || strlen($subject) > 50)
    {
        return_json(array('success' => false, 'error' => 'invalid_subject'));
    }
 
    if (strlen($text) < 3 || strlen($text) > 5000)
    {
        return_json(array('success' => false, 'error' => 'invalid_text'));
    }
   
    if ($_SESSION['captcha'] == $captcha)
    {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: Kontaktny formular <formular@mazanova.sk>' . "\r\n";
        $headers .= 'Reply-To: noreply@mazanova.sk' . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
 
        $message  = '<html>';
        $message .= '<table>';
        $message .= '<tr>';
        $message .= '<td>Meno:</td>';
        $message .= '<td>' . htmlspecialchars($name) . '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td>Predmet:</td>';
        $message .= '<td>' . htmlspecialchars($subject) . '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td>E-mail:</td>';
        $message .= '<td>' . htmlspecialchars($email) . '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td>Text:</td>';
        $message .= '<td>' . htmlspecialchars($text) . '</td>';
        $message .= '</tr>';
        $message .= '</table>';
        $message .= '</html>';
                   
        mail('veronika@mazanova.sk', 'Kontaktny formular', $message, $headers);
		
		return_json(array('success' => true));
    }
    else {
        return_json(array('success' => false, 'error' => 'invalid_captcha'));
    }
}
 
return_json(array('success' => false, 'error' => 'invalid_request'));